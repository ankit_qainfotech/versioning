package test;
public class Elevator
{
  private final int MIN_FLOOR = 1;
  private final int MAX_FLOOR = 5;
  private int currentFloor = 1;
 
  PersonStackElevator ePersonStack; 
  Stack lPersonStack;
  Person[] personList;
   
  private void moveUp() 
  {
    if (currentFloor < MAX_FLOOR)
    {
      currentFloor++;
    }
  }
 
  private void moveDown() 
  {
    if (currentFloor > MIN_FLOOR)
    {
      currentFloor--;
    }
  }
   
  public void move(Person nextPerson, PersonStackElevator ePersonStack)
  {
    System.out.println("Current floor: " + currentFloor);
       
    if (nextPerson.getFloorEntered() > currentFloor) {
      moveUp(); 
    }
    else {
      moveDown();
    }
  }
   
  public void load(Person[] personList, int MAX_PERSONS, PersonStackElevator ePersonStack, Stack lPersonStack)
  {
    for (int j = 0; j < MAX_PERSONS; j++)
    {
      if (personList[j].getFloorEntered() == currentFloor && !ePersonStack.isFull())
      {
        ePersonStack.push(personList[j]);
      }
      else if (personList[j].getFloorEntered() == currentFloor && ePersonStack.isFull())
      {
        System.out.println(personList[j].getName() + " took the stairs.");
        stairsCounter++;
      }
      else
      {
        System.out.println("Next to load: " + personList[j].getName());
         
        move(personList[j], ePersonStack);
        unload(ePersonStack, lPersonStack);
        j--;
      }       
    }
  }
  ...
}